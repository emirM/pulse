export class Membership {
  lang: string;
  url: string;
  token: string;
  account: number;
  price: number;
  object_type: number;
  app_id: number;
  object_pk: number;
}
