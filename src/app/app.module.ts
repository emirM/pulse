import {NgModule} from '@angular/core';
import {IonicApp, IonicModule} from 'ionic-angular';
import {MyApp} from './app.component';
import {HomePage} from '../pages/home/home';
import {TabsPage} from '../pages/tabs/tabs';
import {SearchComponent} from "../pages/search/search";
import {MyListsComponent} from "../pages/mylists/mylists";
import {PharmacyComponent} from "../pages/pharmacy/pharmacy";
import {SettingComponent} from "../pages/settings/setting";
import {JsonpModule} from '@angular/http';
import {MapComponent} from "../pages/map/map";
import {SearchDetail} from "../pages/search/search-detail/search-detail";
import {CommonModule} from "@angular/common";
import {FilterPharmacyComponent} from "../pages/pharmacy/filter/filter-pharmacy";
import {MembershipComponent} from "../pages/membership/membership";

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    TabsPage,
    SearchComponent,
    MyListsComponent,
    PharmacyComponent,
    SettingComponent,
    MapComponent,
    SearchDetail,
    FilterPharmacyComponent,
    MembershipComponent,
  ],
  imports: [
    IonicModule.forRoot(MyApp),
    JsonpModule,
    CommonModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    TabsPage,
    SearchComponent,
    MyListsComponent,
    PharmacyComponent,
    SettingComponent,
    MapComponent,
    SearchDetail,
    FilterPharmacyComponent,
    MembershipComponent,
  ]
})
export class AppModule {
}
