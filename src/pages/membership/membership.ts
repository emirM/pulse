import { Component } from '@angular/core';
import {NavParams, NavController} from "ionic-angular";
import {Membership} from "../../model/membership";

@Component({
  templateUrl: "membership.html"
})
export class MembershipComponent {
  membership: any;

  constructor(private navParams: NavParams) {
    this.membership = navParams.get('membership');
  }
}
