import { Component } from '@angular/core';
import {Platform, AlertController, NavController} from 'ionic-angular';
import {AuthentificateService} from "../../services/authentificate.service";
import {AuthUser} from "../../model/mock/auth-user-mock";
import {HomePage} from "../home/home";
import {Facebook} from 'ionic-native';
import {Membership} from "../../model/membership";
import {MembershipComponent} from "../membership/membership";

@Component({
  templateUrl: 'setting.html',
  providers: [AuthentificateService, Facebook]
})
export class SettingComponent {
  setting: string = "address";
  isAndroid: boolean = false;
  authUser = AuthUser;
  membership: Membership;

  constructor(platform: Platform,
              public alertCtrl: AlertController,
              private authentificateService: AuthentificateService,
              public navCtrl: NavController,
              public navController: NavController) {
    this.isAndroid = platform.is('android');
  }

  getCurrentAuthUser(): void {
    this.authentificateService.getCurrentUser().subscribe(
      data => {
        console.log(data);
        this.authUser.token = data.token;
        this.authUser.user.email = data.user.email;
        this.authUser.user.first_name = data.user.first_name;
        this.authUser.user.last_name = data.user.last_name;
        this.authUser.user.username = data.user.username;
        console.log("authUser: "+this.authUser);
      },
      err => {
        console.log(err);
        this.showInfo("Ошибка!", err);
      },
      () => {
        console.log("Complete!");
      }
    );
  }

  doChangePass(newPass: HTMLInputElement, confirmPass: HTMLInputElement): any {
    if (newPass.value != confirmPass.value) {
      this.showInfo("Ошибка!", "Пароль не совпадает.");
      return;
    }

    this.authentificateService.changePass(newPass.value, confirmPass.value, this.authUser.token).subscribe(
      data => {
        console.log(data);
        console.log("this.authUser: "+this.authUser);
        this.showInfo("Выполнено!", "Пароль успешно изменено.");
        newPass.value = "";
        confirmPass.value = "";
      },
      err => {
        console.log(err);
        this.showInfo("Ошибка", err);
      },
      () => {
        console.log("OK complete!");
      }
    );

  }

  doMembership(companyId: number): any {
    console.log("this.authUser: "+this.authUser.token);
    this.authentificateService.membership(companyId, this.authUser.token).subscribe(
      data => {
        //console.log(data);
        //alert(JSON.stringify(data));
        //alert("data.account: "+data.account);
        this.authentificateService.sendMembership(data).subscribe(
          data => {
            alert(JSON.stringify(data));
          },
          err => {
            alert(err);
          },
          () => {
            console.log("complete!");
          }
        );
      },
      err => {
        alert(err);
      },
      () => {
        console.log("Complete!");
      }
    );
  }

  doLogout(): any {
    this.authentificateService.logout().subscribe(
      data => {
        console.log(data);
        this.showInfo("Успешный!", "Вы вышли.");
        this.authUser.token = "";
        this.authUser.user.first_name = "";
        this.authUser.user.last_name = "";
        this.authUser.user.email = "";
        this.authUser.user.username = "";
        if(this.authUser.user.isLoggedFacebook) {
          this.authUser.user.isLoggedFacebook = false;
          this.logoutFacebook();
        }

        var tabElem: any = document.getElementsByClassName("tabbar");
        tabElem[0].style.display = "none";
        this.navCtrl.push(HomePage);
      },
      err => {
        this.showInfo("Ошибка!", err);
      },
      () => {
        console.log("Complete!");
      }
    );
  }

  logoutFacebook(): void {
    Facebook.logout().then((response) => {
      alert(JSON.stringify(response));
    }, (error) => {
      alert(error);
    });
  }

  showInfo(title: string, text: string): void {
    let alert = this.alertCtrl.create({
      title: title,
      subTitle: text,
      buttons:[
        {
          text: "OK",
          type: "text"
        }
      ]
    });
    alert.present();
  }
}
